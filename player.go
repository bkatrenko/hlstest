package main

import (
	"errors"
	"strconv"
	"time"

	"bitbucket.org/bkatrenko/hlstest/config"
	"bitbucket.org/bkatrenko/hlstest/managers"
	"bitbucket.org/bkatrenko/hlstest/models"
	"bitbucket.org/bkatrenko/hlstest/provider"
	"bitbucket.org/bkatrenko/hlstest/utils"
)

type Player struct {
	options    *managers.OptionsManager
	logManager *managers.LogManager
	buffer     *models.ChunkBuffer
	provider   *provider.Provider
	adapter    *Adapter

	chunkList     []models.Chunk
	playlist      []string
	tag           string
	isWaiting     bool
	number        int
	loadedCounter int
}

func InitPlayer(prov *provider.Provider, playerNumber int,
	opt *managers.OptionsManager, log *managers.LogManager) Player {

	return Player{
		isWaiting:  false,
		number:     playerNumber,
		provider:   prov,
		options:    opt,
		logManager: log,
		tag:        "p" + strconv.Itoa(playerNumber),
		buffer:     new(models.ChunkBuffer)}
}

func (p *Player) prepare(list int) bool {
	var chunks []models.Chunk

	p.buffer.InitBuffer(p.options.BufferSize, config.LOADED_URL_BUFFER_SIZE_CHUNK)

	if p.playlist == nil {
		p.playlist = p.provider.GetPlaylist(p.options.Url, p.tag)
	}

	if p.playlist == nil || len(p.playlist) == 0 {
		p.provider.AppendError(errors.New("Can't init playlist"))
		return false
	}

	chunks, p.options.IsLive = p.adapter.getChunksFromPlaylist(
		p.provider.GetChunks(p.options.Url, p.playlist[list], p.tag))

	if chunks == nil || len(chunks) == 0 {
		p.provider.AppendError(errors.New("Can't init chunklist"))
		return false
	}

	p.options.BaseUrlPart = p.provider.GetBaseUrlPart(p.options.Url) +
		p.adapter.getUrlPrefixFromPlaylist(p.playlist)

	p.chunkList = chunks
	return true
}

func (p *Player) playVideo(list int) {
	p.logManager.LogStartPlaying(p.tag)

	if !p.prepare(list) {
		return
	}

	newChunkChannel, chunkViewedChannel, doneChannel := p.createPlayerChannels()
	p.startVideoBuffering(newChunkChannel)

	for {
		select {
		case result := <-newChunkChannel:
			if result != nil{
				p.buffer.PushBack(result)
			}

			p.loadedCounter++
			if p.options.Chunks != -1 && p.loadedCounter >= p.options.Chunks {
				return
			}

			if p.buffer.Len() == 1 {
				go p.play(chunkViewedChannel, result)
			}

			p.tryToLoadNextChunk(newChunkChannel, doneChannel)

		case <-chunkViewedChannel:
			p.buffer.RemoveFirst()
			p.tryToLoadNextChunk(newChunkChannel, doneChannel)
			go p.play(chunkViewedChannel, p.buffer.GetFront())

		case <-doneChannel:
			p.logManager.LogPlayerStopped(p.tag)
			return
		}
	}
}

func (p *Player) try(channel chan int){
	p.options.Chunks = 1
	p.playlist = p.provider.GetPlaylist(p.options.Url, p.tag)

	if p.playlist == nil || len(p.playlist) == 0 {
		p.provider.AppendError(errors.New("Can't init playlist"))
		channel <- 0
		return
	}

	p.options.Players = len(p.playlist)

	for i:=0; i<len(p.playlist);i++{
		if i<len(p.playlist) {
			go func(i int) {
				p.loadVideo(i)
				channel <- 0
			}(i)
		}
	}
}

func (p *Player) makeSimpleHttp(channel chan int){
	p.provider.MakeSimpleHttp()
	channel <- 0
}


func (p *Player) loadVideo(list int) {
	p.logManager.LogStartPlaying(p.tag)
	if !p.prepare(list) {
		return
	}

	newChunkChannel, _, doneChannel := p.createPlayerChannels()
	p.startVideoBuffering(newChunkChannel)

	for {
		select {
		case <-newChunkChannel:
				p.loadedCounter++
				if p.options.Chunks != -1 && p.loadedCounter >= p.options.Chunks {
					return
				}

				go p.loadNextChunk(newChunkChannel, doneChannel)
			break

		case <-doneChannel:
			p.logManager.LogPlayerStopped(p.tag)
			return
		}
	}
}

func (p *Player) startVideoBuffering(newChunkChannel chan *models.Chunk) {
	go p.startDownloadChunk(newChunkChannel, &p.chunkList[0])
	p.buffer.AddUrl(p.chunkList[0].Url)
}

func (p *Player) tryToLoadNextChunk(newChunkChannel chan *models.Chunk, doneChannel chan int) {
	if p.buffer.Len() >= p.options.BufferSize {
		return
	}

	go p.loadNextChunk(newChunkChannel, doneChannel)
}

func (p *Player) loadNextChunk(newChunkChanel chan *models.Chunk, doneChanel chan int) {
	if p.isWaiting {
		return
	}

	newChunkUrl := p.getNewChunkUrl(doneChanel)

	if newChunkUrl == nil {
		p.logManager.LogOutOfSync(p.tag)
		p.isWaiting = true
		time.Sleep(time.Second * config.WAITING_NEW_PLAYLIST_TIMEOUT_SEC)
		p.isWaiting = false
		p.loadNextChunk(newChunkChanel, doneChanel)
		return
	}

	p.buffer.AddUrl(newChunkUrl.Url)
	p.startDownloadChunk(newChunkChanel, newChunkUrl)
}

func (p *Player) getNewChunkUrl(done chan int) *models.Chunk {
	lastLoadedChunk := p.buffer.GetLastUrl()
	nextChunk := utils.SearchNextChunk(lastLoadedChunk, p.chunkList)

	if nextChunk != nil {
		return nextChunk
	}

	if !p.options.IsLive {
		done <- 0
	}

	return utils.FilterOldChunksAndGetNext(p.buffer.GetUrlBuffer(), p.getNewChunkList())
}

func (p *Player) getNewChunkList() []models.Chunk {
	if p.playlist == nil || len(p.playlist) == 0 {
		return nil
	}

	newChunks, _ := p.adapter.getChunksFromPlaylist(
		p.provider.GetChunks(p.options.Url, p.playlist[0], p.tag))

	return newChunks
}

func (p *Player) startDownloadChunk(onEnd chan *models.Chunk, chunk *models.Chunk) {
	if chunk == nil {
		return
	}

	_, err := p.provider.GetChunk(p.options.BaseUrlPart, chunk.Url, p.tag)
	if err != nil {
		onEnd<-nil
	}

	onEnd <- chunk
}

func (p *Player) play(onEnd chan *models.Chunk, chunk *models.Chunk) {
	if chunk != nil {
		time.Sleep(time.Second * time.Duration(chunk.Time))
		onEnd <- chunk
	}
}

func (p *Player) check() int {
	playlist := p.provider.GetPlaylist(p.options.Url, p.tag)
	var chunks [][]models.Chunk

	if playlist == nil || len(playlist) == 0 {
		p.provider.Errors = append(p.provider.Errors, errors.New("Playlist is nil"))
		p.logManager.LogBadPlaylistState(p.tag)
		return 0
	}

	if len(playlist) == 1 {
		p.provider.Errors = append(p.provider.Errors, errors.New("Playlist contains only one chunklist"))
		p.logManager.LogSinglePlaylistState(p.tag)
		return 0
	}

	for _, v := range playlist {
		newChunks, _ := p.adapter.getChunksFromPlaylist(
			p.provider.GetChunks(p.options.Url, v, p.tag))

		chunks = append(chunks, newChunks)
	}
	return p.checkChunksLength(chunks)
}

func (p *Player) checkChunksLength(list [][]models.Chunk) int {
	var incCounter int

	for _, v := range list {
		if len(v) != len(list[0]) {
			p.logManager.LogBadListLength()
			return incCounter
		}
	}

	for k, v := range list[0] {
		for i := 1; i < len(list); i++ {
			if list[i][k].Time != v.Time {
				p.logManager.LogIncompatibilityDetected()
				incCounter++
			}
		}
	}
	p.logManager.LogCheckResult(incCounter)
	return incCounter
}

func (p *Player) createPlayerChannels() (chan *models.Chunk, chan *models.Chunk, chan int) {
	newChunkChan := make(chan *models.Chunk)
	viewedChunkChan := make(chan *models.Chunk)
	taskEndChan := make(chan int)
	return newChunkChan, viewedChunkChan, taskEndChan
}
