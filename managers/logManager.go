package managers

import (
	"io"
	"log"
	"net/http"
	"os"
	"runtime"
	"strconv"
	"sync"
	"time"

	"bitbucket.org/bkatrenko/hlstest/config"
	"github.com/fatih/color"
)

type LogManager struct {
	logger  *log.Logger
	options *OptionsManager
	mu      sync.Mutex
}

func (l *LogManager) init() {
	var writer io.Writer
	if runtime.GOOS == "windows" {
		writer = color.Output
	} else {
		writer = os.Stdout
	}

	l.logger = log.New(writer, "", 0)
}

func (l *LogManager) setLogPrefics(msg string, color func(string, ...interface{}) string) {
	if msg != "." {
		l.logger.SetPrefix(color(time.Now().Format("15:04:05.000") + " "))
	} else {
		l.logger.SetPrefix("")
	}
}

func (l *LogManager) Log(msg string, colorFunc func(string, ...interface{}) string) {
	l.mu.Lock()
	l.setLogPrefics(msg, colorFunc)
	l.logger.Print(colorFunc(msg))
	l.mu.Unlock()
}

func (this *LogManager) SetOptions(options *OptionsManager) {
	this.options = options
	this.init()
}

func (l *LogManager) getKbps(since time.Time, body []byte) string {
	if body != nil {
		size := int64(len(body))
		time := time.Since(since).Nanoseconds() / config.NANOSECOND_IN_MILLIS
		return strconv.FormatInt(size/time, config.DEMICAL)
	}
	return ""
}

func (l *LogManager) LogError(id string, since time.Time, err error) string {
	if err != nil {
		return color.RedString("ERROR done " + id + ":" + " with error: " + err.Error())
	} else {
		return color.CyanString("INFO done " + id + ":")
	}
}

func (l *LogManager) LogResponse(errorLog string, since time.Time, response *http.Response) string {
	if response != nil {
		var code string
		errorLog += color.CyanString(" code=")
		if response.StatusCode == 200 {
			code = color.GreenString(strconv.Itoa(response.StatusCode))
		} else {
			code = color.RedString(strconv.Itoa(response.StatusCode))
		}

		if response.StatusCode == 200 {
			errorLog = color.CyanString(errorLog) + code
		} else {
			errorLog = color.RedString(errorLog) + code
		}
	}

	sinceTime := time.Since(since).Nanoseconds() / config.NANOSECOND_IN_MILLIS

	if sinceTime > config.OVERTIME_EDGE_MS {
		l.Log("WARNING - overtime detected", color.YellowString)
	}


	if response != nil && response.StatusCode == 200{
		return errorLog + color.CyanString(" time="+
			strconv.FormatInt(sinceTime, config.DEMICAL))
	} else {
		return errorLog + color.RedString(" time="+
			strconv.FormatInt(sinceTime, config.DEMICAL))
	}
}

func (l *LogManager) LogResponceHelperData(responseLog string, since time.Time,
	body []byte) string {
	if body != nil {
		responseLog +=
			color.CyanString(" bytes="+strconv.Itoa(len(body))) +
				color.CyanString(" kbps="+l.getKbps(since, body))
	}
	return responseLog
}

func (l *LogManager) LogResponseHeaders(responseLog string, response *http.Response, isPrintBody bool) string {
	if response != nil && isPrintBody{
		responseLog += "\n"
		for k, v := range response.Header {
			responseLog += k + ": " + v[0] + "\n"
		}
	}
	return responseLog
}

func (l *LogManager) LogResponseBody(responseLog string, body []byte, isPrintBody bool) string {
	if body != nil && isPrintBody {
		responseLog += "\n"
		responseLog += string(body)
	}
	return responseLog
}

func (l *LogManager) LogRequestState(id string, since time.Time, body []byte,
	response *http.Response, isPrintBody bool, err error) {
	var colorFunc func(string, ...interface{}) string

	if err == nil {
		colorFunc = color.CyanString
	} else {
		colorFunc = color.RedString
	}

	if l.options.Verbose {
		l.Log(
			l.LogResponseBody(
				l.LogResponseHeaders(
					l.LogResponceHelperData(
						l.LogResponse(
							l.LogError(id, since, err), since, response),
						since, body), response, isPrintBody), body, isPrintBody), colorFunc)
	}
}

func (l *LogManager) LogStartInfoFetching(id string, url string) {
	if l.options.Verbose {
		l.Log(color.CyanString("INFO fetching "+id+": "+url), color.CyanString)
	}
}

func (l *LogManager) LogStartPlaying(tag string) {
	if l.options.Verbose {
		l.Log(color.CyanString("INFO "+tag+" player started"), color.CyanString)
	}
}

func (l *LogManager) LogBadPlaylistState(tag string) {
	if l.options.Verbose {
		l.Log(color.RedString("ERROR "+tag+" can't init chunklist"), color.RedString)
	}
}

func (l *LogManager) LogSinglePlaylistState(tag string) {
	if l.options.Verbose {
		l.Log(color.RedString("ERROR "+tag+" playlist contains only one chunklist"), color.RedString)
	}
}

func (l *LogManager) LogPlayerStopped(tag string) {
	if l.options.Verbose {
		l.Log(color.CyanString("DEBUG "+tag+" player stoped (end of playlist)"), color.CyanString)
	}
}

func (l *LogManager) LogOutOfSync(tag string) {
	if l.options.Verbose {
		l.Log(color.CyanString("DEBUG "+tag+
			"out of sync - mast waiting for new chunk. Sleep 10s"), color.CyanString)
	}
}

func (l *LogManager) LogBadListLength() {
	l.Log(color.RedString("ERROR Length of the list does not equal"), color.RedString)
}

func (l *LogManager) LogIncompatibilityDetected() {
	l.Log(color.YellowString("WARNING Incompatibility detected"), color.YellowString)
}

func (l *LogManager) LogCheckResult(incCounter int) {
	if incCounter > 0 {
		l.Log(color.YellowString("WARNING Find "+strconv.Itoa(incCounter)+" incompatibilitys"), color.YellowString)
	} else {
		l.Log(color.GreenString("No incompatibilitys"), color.GreenString)
	}
}

func (l *LogManager) PrintProviderStatistic(errors []error, bytes int64, chunks int, chunklists int) {
	colorFunc := l.getStatisticLogColor(len(errors) > 0)

	if len(errors) > 0 {
		l.Log(strconv.Itoa(len(errors))+" errors detected", colorFunc)
		for _, v := range errors {
			l.Log(v.Error(), colorFunc)
		}
	}
	var logString string

	logString += "byte-fetched=" + strconv.FormatInt(bytes, 10) +
		" chunks-fetched=" + strconv.Itoa(chunks) +
		" chunklists-fetched=" + strconv.Itoa(chunklists)
	l.Log(logString, colorFunc)

	if len(errors) == 0 {
		l.Log("OK", colorFunc)
	} else {
		l.Log("ERROR", colorFunc)
	}
}

func (l *LogManager) LogTimePassed(start time.Time, errors []error) {
	colorFunc := l.getStatisticLogColor(len(errors) > 0)
	l.Log("=========================================", colorFunc)
	l.Log("time-passed="+strconv.FormatFloat(time.Since(start).Seconds(), 'f', 1, 64)+"s",
		colorFunc)
}

func (l *LogManager) getStatisticLogColor(isErrors bool) func(string, ...interface{}) string {
	if isErrors {
		return color.RedString
	} else {
		return color.GreenString
	}
}
