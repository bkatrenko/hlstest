package managers

import (
	"log"
	"strconv"
	"strings"
	"time"
	"os"
	"github.com/joho/godotenv"
	"bitbucket.org/bkatrenko/hlstest/utils"
)

type OptionsManager struct {
	Url            string
	BaseUrlPart    string

	Verbose        bool
	IsShowBody     bool
	IsShowDots     bool

	Mode           string
	Chunks         int
	Players        int
	BufferSize     int
	Time           float64
	IsLive         bool
	UntilError     bool
	time.Duration
	Interval       int
	ForPlayingTime int
}

func (o *OptionsManager) initDefaultOptions(path string) {
	o.fillDefaults()

	err := godotenv.Load(path)
	if err != nil {
		utils.CreateFile(path)
		o.writeDefaultEnv()
		return
	}
	o.getDefaultEnv()
}

func (o *OptionsManager) writeDefaultEnv(){
	utils.WriteFile("config.env", "mode=try\n" +
			"verbose=true\n" +
			"isShowBody=true\n" +
			"isShowDots=true\n" +
			"chunks=-1\n" +
			"players=1\n" +
			"bufferSize=3\n" +
			"forPlayingTime=-1\n" +
			"interval=10\n" +
			"untilError=false\n" +
			"duration=second\n")
}

func (o *OptionsManager) getDefaultEnv(){
	verbose, _ := strconv.ParseBool(os.Getenv("verbose"))
	o.Verbose = verbose

	isShowBody, _ := strconv.ParseBool(os.Getenv("isShowBody"))
	o.IsShowBody = isShowBody

	isShowDots, _ := strconv.ParseBool(os.Getenv("isShowDots"))
	o.IsShowDots = isShowDots
		mode := os.Getenv("mode")
	if mode != "" && (mode == "play" || mode == "check" || mode == "try" || mode == "download") {
		o.Mode = mode
	}

	bufferSize, _ := strconv.Atoi(os.Getenv("bufferSize"))
		o.BufferSize = bufferSize

	players, _ := strconv.Atoi(os.Getenv("players"))
		o.Players = players

	chunks, _ := strconv.Atoi(os.Getenv("chunks"))
		o.Chunks = chunks

	forPlayingTime, _ := strconv.Atoi(os.Getenv("forPlayingTime"))
		o.ForPlayingTime = forPlayingTime

	interval, _ := strconv.Atoi(os.Getenv("interval"))
		o.Interval = interval

	untilError, _ := strconv.ParseBool(os.Getenv("untilError"))
		o.UntilError = untilError

	duration := os.Getenv("duration")
	if (duration != "" && (duration=="minute" || duration=="second" || duration=="hour")){
		if duration == "minute" {
			o.Duration = time.Minute
		}

		if duration == "second" {
			o.Duration = time.Second
		}

		if (duration == "hour") {
			o.Duration = time.Hour
		}
	}
}

func (o *OptionsManager) fillDefaults(){
	o.Verbose = true
	o.IsShowBody = true
	o.IsShowDots = true
	o.Mode = "try"
	o.BufferSize = 3
	o.Players = 1
	o.Chunks = -1

	o.ForPlayingTime = -1
	o.Interval = 10
	o.Duration = time.Second
	o.UntilError = false
}

func getConfigPath(options []string) string{
	pathResult := "config.env"

	for k, v := range options {
	    if v == "configfile"{
		    if len(options) > k+1{
			    pathResult = options[k+1]
		    }
	    }
	}
	return pathResult
}

func (o *OptionsManager) ParseOptions(options []string) {
	defer func() {
		if r := recover(); r != nil {
			log.Println("oh, panic while parsing args :(")
		}
	}()

	o.initDefaultOptions(getConfigPath(options))

	if options != nil {
		for k, v := range options {
			if strings.Contains(v, "http") {
				o.Url = v
			}

			if v == "play" || v == "download" || v == "check" || v == "try" || v == "http" {
				o.Mode = v
			}

			if v == "buffer" {
				if len(options) > k+1 {
					o.BufferSize, _ = strconv.Atoi(options[k+1])
				}
			}

			if v == "chunk" || v == "chunks" {
				o.Chunks, _ = strconv.Atoi(options[k-1])
			}

			if v == "player" || v == "players" {
				o.Players, _ = strconv.Atoi(options[k-1])
			}

			if v == "silent" {
				o.Verbose = false
			}

			if v == "verbose" {
				o.Verbose = true
			}

			if (v == "minutes" || v == "minute" || v == "min" || v == "m") &&
				(options[k-2] == "for") {
				o.Duration = time.Minute
				o.ForPlayingTime, _ = strconv.Atoi(options[k-1])
			}

			if (v == "seconds" || v == "second" || v == "sec" || v == "s") &&
				(options[k-2] == "for") {
				o.Duration = time.Second
				o.ForPlayingTime, _ = strconv.Atoi(options[k-1])
			}

			if (v == "hour" || v == "hours" || v == "h") &&
				(options[k-2] == "for") {
				o.Duration = time.Hour
				o.ForPlayingTime, _ = strconv.Atoi(options[k-1])
			}

			if v == "each" {
				if len(options) > k+1 {
					o.Interval, _ = strconv.Atoi(options[k+1])
				}
			}

			if v == "until" {
				if len(options) > k+1 {
					if options[k+1] == "error" {
						o.UntilError = true
					}
				}
			}

			if v == "body" {
				if (options[k-1] == "no"){
					o.IsShowBody = false
				}
			}

			if v == "dots" {
				if (options[k-1] == "no"){
					o.IsShowDots = false
				}
			}
		}
	}

	if o.Verbose {
		log.Println("Options: " + o.getMode()+ " " + o.Url + " " +
			o.getChunksCount() + " with " + strconv.Itoa(o.Players) + " players ")
	}
}

func (o *OptionsManager) getLogMode() string {
	if o.Verbose {
		return "verbose"
	} else {
		return "silent"
	}
}

func (o *OptionsManager) getMode() string {
	return o.Mode
}

func (o *OptionsManager) getChunksCount() string {
	if o.Chunks == -1 {
		return "forever"
	} else {
		return strconv.Itoa(o.Chunks) + " " + "chunks"
	}
}
