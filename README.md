Main goal of this app is help media developers test media servers like WOWZA. App works like a player - download chunks, request new chunk lists, buffering some info, and show all info to std out - list of chunks, size, speed, errors, warnings etc. You can set number of players - and do some kind of load testing

Just build it with go build, start app without any args and view help info.

Main options:
> - play - playback imitation
> - download - fast downloading files
> - check - check chunks legth
> - try - play 1 chunk

> - Helpers:
> - for [count] s(sec)(second(s))
> - for [count] m(min)(minute(s))
> - for [count] h(hour)(hours(s)) - set time period
> - silent | verbose - logging options

> - Examples:
> - hlstest version
> - make only get request - hlstest http http://host:port/vod/toplay/playlist.m3u8

> - hlstest try http://host:port/vod/toplay/playlist.m3u8
> - hlstest play http://host:port/vod/toplay/playlist.m3u8
> - hlstest play http://host:port/vod/toplay/playlist.m3u8 with 20 players
> - hlstest play 3 chunk http://host:port/vod/toplay/playlist.m3u8
> - hlstest download 3 chunk http://host:port/vod/toplay/playlist.m3u8
> - hlstest play for 30 sec http://host:port/vod/toplay/playlist.m3u8
> - hlstest check http://host:port/vod/toplay/playlist.m3u8 for 1 min each 10 s
> - hlstest play http://host:port/vod/toplay/playlist.m3u8 configfile [file path]