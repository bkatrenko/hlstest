package main

import (
	"os"

	"runtime"

	"bitbucket.org/bkatrenko/hlstest/managers"
	"bitbucket.org/bkatrenko/hlstest/utils"
	interrupts "github.com/rshmelev/go-inthandler"
)

// stream examples
//http://g33ktricks.blogspot.com.br/2016/04/list-of-hls-streaming-video-sample-test.html

func init() {
	runtime.GOMAXPROCS(runtime.NumCPU())
}

func main() {
	controller := &Controller{}
	options := &managers.OptionsManager{}
	interrupts.TakeCareOfInterrupts(false)
	checkNoPlayingMode(controller)

	options.ParseOptions(os.Args)
	endChannel := controller.startDoingTask(options)

	for i := 0; i < options.Players; i++ {
		select {
		case <-endChannel:
		case <-interrupts.StopChannel:
			break
		}
	}

	exit(controller)
}

func checkNoPlayingMode(controller *Controller) {
	if len(os.Args) < 2 {
		controller.showHelp()
		os.Exit(0)
	}

	if utils.IsNeedVersion(os.Args) {
		controller.showVersion()
		os.Exit(0)
	}
}
