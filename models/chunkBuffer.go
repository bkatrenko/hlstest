package models

import (
	"container/list"
)

type ChunkBuffer struct {
	loadedBuffer *list.List
	ulrsBuffer   []string

	loadedBufferSize int
	urlBufferSize    int
}

func (cb *ChunkBuffer) InitBuffer(maxLoadedBufferSize int, maxUrlBufferSize int) {
	cb.loadedBufferSize = maxLoadedBufferSize
	cb.urlBufferSize = maxUrlBufferSize

	cb.loadedBuffer = list.New()
}

func (cb *ChunkBuffer) PushBack(chunk *Chunk) {
	cb.loadedBuffer.PushBack(chunk)
}

func (cb *ChunkBuffer) RemoveFirst() {
	if cb.loadedBuffer.Len() > 0 {
		cb.loadedBuffer.Remove(cb.loadedBuffer.Front())
	}
}

func (cb *ChunkBuffer) GetFront() *Chunk {
	if cb.Len() > 0 {
		return cb.loadedBuffer.Front().Value.(*Chunk)
	} else {
		return nil
	}
}

func (cb *ChunkBuffer) Len() int {
	return cb.loadedBuffer.Len()
}

func (cb *ChunkBuffer) AddUrl(url string) {
	if len(cb.ulrsBuffer) < cb.urlBufferSize {
		cb.ulrsBuffer = append(cb.ulrsBuffer, url)
	} else {
		cb.ulrsBuffer = cb.ulrsBuffer[1:]
		cb.ulrsBuffer = append(cb.ulrsBuffer, url)
	}
}

func (cb *ChunkBuffer) GetUrlBuffer() []string {
	return cb.ulrsBuffer
}

func (cb *ChunkBuffer) GetLastUrl() string {
	if len(cb.ulrsBuffer) > 0 {
		return cb.ulrsBuffer[len(cb.ulrsBuffer)-1]
	} else {
		return ""
	}
}
