package models

type Chunk struct {
	Url  string
	Time float64
}
