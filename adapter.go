package main

import (
	"strconv"
	"strings"

	"bitbucket.org/bkatrenko/hlstest/models"
)

type Adapter struct {
}

func (a *Adapter) getUrlPrefixFromPlaylist(playlists []string) string {
	if len(playlists) > 0 && strings.Contains(playlists[0], "/") &&
		!strings.Contains(playlists[0], "http://") &&
		!strings.Contains(playlists[0], "https://") {
		return playlists[0][0 : strings.LastIndex(playlists[0], "/")+1]
	} else {
		return ""
	}
}

func (a *Adapter) getChunksFromPlaylist(body []byte) ([]models.Chunk, bool) {
	list, isLive := a.getChunksName(string(body))
	return list, !isLive
}

func (a *Adapter) getChunksName(body string) ([]models.Chunk, bool) {
	parts := strings.Fields(body)
	var time float64
	var chunks []models.Chunk

	for _, v := range parts {
		if strings.Contains(v, "EXTINF") {
			time, _ = strconv.ParseFloat(v[strings.LastIndex(v, ":")+1:strings.LastIndex(v, ",")], 64)
		}

		if strings.Contains(v, ".ts") || strings.Contains(v, ".aac") {

			chunks = append(chunks, models.Chunk{v, time})
			time = 0
		}
	}
	return chunks, strings.Contains(body, "#EXT-X-ENDLIST")
}
