package utils

import (
	"strings"

	"bitbucket.org/bkatrenko/hlstest/models"
	"os"
)

func FilterOldChunksAndGetNext(oldList []string, newList []models.Chunk) *models.Chunk {
	var filteredList []models.Chunk

	if oldList == nil || newList == nil {
		return nil
	}

	for _, v := range newList {
		if !IsContains(oldList, v) {
			filteredList = append(filteredList, v)
		}
	}

	if len(filteredList) == 0 {
		return nil
	}

	return &filteredList[0]
}

func IsContains(list []string, chunk models.Chunk) bool {
	for _, v := range list {
		if strings.Contains(v, chunk.Url[strings.LastIndex(chunk.Url, "_"):strings.LastIndex(chunk.Url, ".")]) {
			return true
		}
	}
	return false
}

func SearchNextChunk(lastLoadedChunk string, chunkList []models.Chunk) *models.Chunk {
	for k, v := range chunkList {
		if v.Url == lastLoadedChunk {
			if k < len(chunkList)-1 {
				return &chunkList[k+1]
			}
		}
	}
	return nil
}

func CreateFile(path string) {
	 _, err := os.Stat(path)

	if os.IsNotExist(err) {
		var file, err = os.Create(path)
		checkError(err)
		defer file.Close()
	}
}

func WriteFile(path string, info...string) {
	file, err := os.OpenFile(path, os.O_RDWR, 0644)
	checkError(err)
	defer file.Close()

	for _,v:=range info {
		_, err = file.WriteString(v)
	}

	checkError(err)
	err = file.Sync()
	checkError(err)
}

func IsNeedVersion(options []string) bool {
	for _, v := range options {
		if v == "version"{
			return true
		}
	}
	return false
}

func checkError(err error) {
	if err != nil {
		os.Exit(0)
	}
}

