package config

//cofig
const LOADED_URL_BUFFER_SIZE_CHUNK = 20
const WAITING_NEW_PLAYLIST_TIMEOUT_SEC = 10
const HTTP_TIME_OUT_SEC = 120

const VERSION_CODE = " v0.4 alpha"
const PRODUCT_NAME = " hls tester"

//other const
const NANOSECOND_IN_MILLIS = 1000000
const DEMICAL = 10
const OVERTIME_EDGE_MS = 10000