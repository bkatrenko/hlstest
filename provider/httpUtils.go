package provider

import (
	"io/ioutil"
	"net/http"
	"time"

	"bitbucket.org/bkatrenko/hlstest/managers"
	"bitbucket.org/bkatrenko/hlstest/config"
	"errors"
)

type HttpUtils struct {
	logManager managers.LogManager
}

func (h *HttpUtils) SetOptions(options *managers.OptionsManager) {
	h.logManager.SetOptions(options)
}

func (h *HttpUtils) makeRequest(url string, id string, isPrintBody bool) ([]byte, error) {
	h.logManager.LogStartInfoFetching(id, url)


	client := http.Client{
		Timeout: time.Duration(config.HTTP_TIME_OUT_SEC * time.Second),
	}

	start_time := time.Now()
	response, err := client.Get(url)

	if response != nil {
		defer response.Body.Close()
	}

	if err != nil {
		h.logManager.LogRequestState(id, start_time, nil, response, isPrintBody, err)
		return nil, err
	}

	if response != nil && response.StatusCode == http.StatusNotFound{
		h.logManager.LogRequestState(id, start_time, nil, response, isPrintBody, err)
		return nil, errors.New("not found")
	}

	bytes, err := ioutil.ReadAll(response.Body)
	if err != nil {
		h.logManager.LogRequestState(id, start_time, bytes, response, isPrintBody, err)
		return nil, err
	}
	h.logManager.LogRequestState(id, start_time, bytes, response, isPrintBody, err)
	return bytes, err
}
