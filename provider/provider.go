package provider

import (
	"strconv"
	"strings"
	"bitbucket.org/bkatrenko/hlstest/managers"
	"log"
	"sync"
)

type Provider struct {
	HttpUtils  *HttpUtils
	Options    *managers.OptionsManager
	id         int
	mu sync.Mutex
	Errors     []error
	Bytes      int64
	Chunks     int
	Chunklists int
}

func (p *Provider) SetOptions(options *managers.OptionsManager){
	p.Options = options
}

func (p *Provider) GetPlaylist(url string, playerPrefix string) []string {
	body, err := p.HttpUtils.makeRequest(url, p.generateTag(playerPrefix), p.Options.IsShowBody)
	if err != nil {
		p.Errors = append(p.Errors, err)
		return nil
	}

	p.mu.Lock()
	p.Bytes += int64(len(body))
	p.mu.Unlock()
	return getPlaylistsName(url, string(body))
}

func (p *Provider) GetChunks(mainUrl string, playlistUrl string, playerPrefix string) []byte {
	body, err := p.HttpUtils.makeRequest(p.createUrlForGettingChunkList(mainUrl, playlistUrl), p.generateTag(playerPrefix), p.Options.IsShowBody)
	if err != nil {
		p.mu.Lock()
		p.Errors = append(p.Errors, err)
		p.mu.Unlock()
		return nil
	}
	p.mu.Lock()
	p.Chunklists++
	p.mu.Unlock()
	return body
}

func (p *Provider) createUrlForGettingChunkList(mainUrl string, playlistUrl string) string {
	if !strings.Contains(playlistUrl, "http://") && !strings.Contains(playlistUrl, "https://"){
		return 	p.GetBaseUrlPart(mainUrl)+playlistUrl
	} else {
		return playlistUrl
	}
}

func (p *Provider) createUrlForGettingChunk(mainUrl string, chunkUrl string) string {
	if !strings.Contains(chunkUrl, "http://") && !strings.Contains(chunkUrl, "https://"){
		return 	mainUrl + chunkUrl
	} else {
		return chunkUrl
	}
}


func (p *Provider) GetChunk(url string, chunk string, tag string) ([]byte, error) {
	body, err := p.HttpUtils.makeRequest(p.createUrlForGettingChunk(url, chunk), p.generateTag(tag), false)

	if err != nil {
		p.mu.Lock()
		p.Errors = append(p.Errors, err)
		p.mu.Unlock()
		return nil, err
	}

	if body != nil {
		p.mu.Lock()
		p.Bytes += int64(len(body))
		p.Chunks++
		p.mu.Unlock()
	}

	return body, nil
}

func getPlaylistsName(mainUrl string, body string) []string {
	var playlists []string

	if body == ""{
		log.Print("playlist body is void")
		return nil
	}

	if strings.Contains(body, "#EXTINF"){
		log.Print("this is chunklist")
		playlists = append(playlists, mainUrl)
		return playlists
	}


	parts := strings.Fields(body)
	for _, v := range parts {
		if strings.Contains(v, ".m3u8") {
			if strings.Contains(v, "URI=") {
				v = v[strings.LastIndex(v, "URI="):strings.LastIndex(v, ".m3u8")]
			} else {
				playlists = append(playlists, v)
			}
		}
	}

	return playlists
}

func (p *Provider) MakeSimpleHttp(){
	_, err := p.HttpUtils.makeRequest(p.Options.Url, p.generateTag("-"), p.Options.IsShowBody)
	if err != nil {
		p.mu.Lock()
		p.Errors = append(p.Errors, err)
		p.mu.Unlock()
	}
	p.mu.Lock()
	p.Chunklists++
	p.mu.Unlock()
}

func (p *Provider) GetBaseUrlPart(url string) string {
	return url[0 : strings.LastIndex(url, "/")+1]
}

func (p *Provider) AppendError(err error){
	p.Errors = append(p.Errors, err)
}

func (p *Provider) generateTag(playerPrefix string) string {
	p.id++
	return playerPrefix + ": @" + strconv.Itoa(p.id)
}
