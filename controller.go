package main

import (
	"time"

	"bitbucket.org/bkatrenko/hlstest/managers"
	"bitbucket.org/bkatrenko/hlstest/provider"
	"github.com/fatih/color"
	"log"
	"os"
	"bitbucket.org/bkatrenko/hlstest/config"
)

type Controller struct {
	provider   *provider.Provider
	httpUtils  *provider.HttpUtils
	logManager *managers.LogManager
	options    *managers.OptionsManager

	start         time.Time
	playerCounter int
}

func (c *Controller) init(options *managers.OptionsManager, channel chan int) {
	c.options = options

	c.httpUtils = new(provider.HttpUtils)
	c.httpUtils.SetOptions(options)

	c.logManager = new(managers.LogManager)
	c.logManager.SetOptions(options)

	c.provider = new(provider.Provider)
	c.provider.SetOptions(options)

	c.provider.HttpUtils = c.httpUtils
	c.start = time.Now()

	if options.ForPlayingTime > 0 {
		go func() {
			time.Sleep(time.Duration(options.ForPlayingTime) * options.Duration)
			channel <- 0
		}()
	}

	if options.Verbose && options.IsShowDots {
		go c.startLogDots()
	}
}

func (c *Controller) startDoingTask(options *managers.OptionsManager) chan int {
	endTaskChannel := make(chan int)

	switch {
	case options.Mode == "try":
		c.try(options, 0, endTaskChannel)

	case options.Mode == "http":
		c.makeSimpleHttp(options, 0, endTaskChannel)

	case options.Mode == "play":
		c.startPlayers(options, endTaskChannel, (*Player).playVideo)

	case options.Mode == "download":
		c.startPlayers(options, endTaskChannel, (*Player).loadVideo)

	case options.Mode == "check":
		if options.ForPlayingTime > 0 {
			go c.startCheckWithInterval(options, endTaskChannel)
		} else {
			c.startChecker(options, endTaskChannel)
			exit(c)
		}
	}
	return endTaskChannel
}

func (c *Controller) startPlayers(options *managers.OptionsManager, channel chan int, action func(*Player, int)) {
	c.init(options, channel)
	for i := 0; i < options.Players; i++ {
		go c.startAction(i, channel, action)
	}
}

func (c *Controller) startAction(playerNumber int, channel chan int, action func(*Player, int)) {
	player := InitPlayer(c.provider, playerNumber, c.options, c.logManager)
	action(&player, 0)
	channel <- 0
}

func (c *Controller) startChecker(options *managers.OptionsManager, channel chan int) {
	c.init(options, channel)
	c.check(options, 0, channel)
}

func (c *Controller) check(options *managers.OptionsManager,
	playerNumber int, channel chan int) {
	player := InitPlayer(c.provider, playerNumber, options, c.logManager)
	player.check()
}

func (c *Controller) try(options *managers.OptionsManager,
playerNumber int, channel chan int) {
	c.init(options, channel)
	player := InitPlayer(c.provider, playerNumber, options, c.logManager)
	go player.try(channel)
}

func (c *Controller) makeSimpleHttp(options *managers.OptionsManager,
	playerNumber int, channel chan int){
	c.init(options, channel)
	player := InitPlayer(c.provider, playerNumber, options, c.logManager)
	go player.makeSimpleHttp(channel)

}

func (c *Controller) startCheckWithInterval(options *managers.OptionsManager, chanel chan int) {
	c.init(options, chanel)

	for {
		player := InitPlayer(c.provider, 0, options, c.logManager)
		errors := player.check()
		if errors > 0 && c.options.UntilError {
			return
		}

		time.Sleep(time.Second * time.Duration(options.Interval))
	}
}

func (c *Controller) printStatistic() {
	c.logManager.LogTimePassed(c.start, c.provider.Errors)
	c.logManager.PrintProviderStatistic(c.provider.Errors, c.provider.Bytes,
		c.provider.Chunks, c.provider.Chunklists)
}

func (c *Controller) startLogDots() {
	for {
		c.logManager.Log(".", color.MagentaString)
		time.Sleep(time.Second)
	}
}

func exit(c *Controller) {
		c.printStatistic()
		os.Exit(len(c.provider.Errors))
}

func (c *Controller) showHelp() {
	log.Println("Welcome to hls tester")
	log.Println("Main options:")
	log.Println("play - playback imitation")
	log.Println("download - fast downloading files")
	log.Println("check - check chunks legth")
	log.Println("try - play 1 chunk")

	log.Println("Helpers:")
	log.Println("for [count] s(sec)(second(s))")
	log.Println("for [count] m(min)(minute(s))")
	log.Println("for [count] h(hour)(hours(s)) - set time period")
	log.Println("silent | verbose - logging options")

	log.Println("Examples:")
	log.Println("hlstest version")
	log.Println("make only get request - \n hlstest http http://host:port/vod/toplay/playlist.m3u8\n")

	log.Println("hlstest try http://host:port/vod/toplay/playlist.m3u8")
	log.Println("hlstest play http://host:port/vod/toplay/playlist.m3u8")
	log.Println("hlstest play http://host:port/vod/toplay/playlist.m3u8 with 20 players")
	log.Println("hlstest play 3 chunk http://host:port/vod/toplay/playlist.m3u8")
	log.Println("hlstest download 3 chunk http://host:port/vod/toplay/playlist.m3u8")
	log.Println("hlstest play for 30 sec http://host:port/vod/toplay/playlist.m3u8")
	log.Println("hlstest check http://host:port/vod/toplay/playlist.m3u8 for 1 min each 10 s")
	log.Println("hlstest play http://host:port/vod/toplay/playlist.m3u8 configfile [file path]")
}

func (c *Controller) showVersion() {
	log.Println(config.PRODUCT_NAME)
	log.Println(config.VERSION_CODE)
}
